from django.urls import path

from bio_tech.views import WormList, WormDetail

urlpatterns = [
    path('list/', WormList.as_view()),
    path('<int:pk>/', WormDetail.as_view()),
    ]
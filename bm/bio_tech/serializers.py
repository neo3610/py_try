#serializers.py
from rest_framework import serializers

from bio_tech.models import Worm, ParametrsWorm


class WormSerializer(serializers.ModelSerializer):
    class Meta:
        fields ='__all__'
        model = Worm
        depth = 2

# этот код не нужен:
class ParametrsWormSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = ParametrsWorm
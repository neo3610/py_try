from django.apps import AppConfig


class BioTechConfig(AppConfig):
    name = 'bio_tech'

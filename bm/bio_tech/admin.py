from django.contrib import admin

from bio_tech.models import Nematoda, ParametrsWorm, Worm, TypesOfWorm, SubTypesOfWorm

admin.site.register(Nematoda)
admin.site.register(TypesOfWorm)
admin.site.register(SubTypesOfWorm)
admin.site.register(Worm)
admin.site.register(ParametrsWorm)

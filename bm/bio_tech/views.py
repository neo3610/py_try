from rest_framework import generics

from bio_tech.models import Worm, ParametrsWorm
from bio_tech.serializers import WormSerializer, ParametrsWormSerializer


class WormList(generics.ListCreateAPIView):
    def get_queryset(self):
        queryset = Worm.objects.all()

        symbols = self.request.query_params.get('symbols', None)
        if symbols is not None:
            try:
                # symbols = symbols.upper()
                # symbols = symbols.replace('Ч', 'ч')
                # symbols = symbols.replace('Е', 'е')

                queryset = queryset.filter(name__icontains=symbols).order_by('name')
            except ValueError:
                pass
        return queryset

    serializer_class = WormSerializer


class WormDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Worm.objects.all()
    serializer_class = WormSerializer
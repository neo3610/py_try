from django.shortcuts import render
from rest_framework import generics

from students_life.models import Student, Room, StudentInRoom
from students_life.serializers import StudentSerializer, RoomSerializer, StudentInRoomSerializer


class StudentList(generics.ListCreateAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class StudentDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer


class RoomList(generics.ListCreateAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer


class RoomDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Room.objects.all()
    serializer_class = RoomSerializer


class StudentInRoomDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = StudentInRoom.objects.all()
    serializer_class = StudentInRoomSerializer


class StudentInRoomList(generics.ListCreateAPIView):
    def get_queryset(self):
        queryset = StudentInRoom.objects.all()
        name = self.request.query_params.get('name', None)
        capacity = self.request.query_params.get('people', None)

        try:
            capacity = int(capacity)
        except ValueError:
            capacity = 0



        try:
            name = name.upper()
        except BaseException:
            name = ''

        print(name, type(name))
        students_queryset = Student.objects.filter(
            fio__contains=name).first()  # первое вхождение, которое встретил ORM при обращении к БД
        print('students_queryset', students_queryset)
        print('------------------------------------------------------')

        students_queryset = Student.objects.filter(
            fio__contains=name).all()  # все вхождение, которое встретил ORM при обращении к БД
        for student in students_queryset:
            print('students_queryset', student)
        print('------------------------------------------------------')



        for student in students_queryset:
            the_student_in_room = StudentInRoom.objects.filter(id=student.id).first()
            print(the_student_in_room)
            print('for in for')
            rooms_capacity_of_this_student = StudentInRoom.objects.filter(id=student.id).filter(room__capacity=capacity).all()
            for room_capacity in rooms_capacity_of_this_student:
                print(room_capacity)

        print('-------------------++++++++++++++++++-----------------')
        return queryset

    serializer_class = StudentInRoomSerializer

# class WormList(generics.ListCreateAPIView):
#     def get_queryset(self):
#         queryset = Worm.objects.all()
#
#         symbols = self.request.query_params.get('symbols', None)
#         if symbols is not None:
#             try:
#                 # symbols = symbols.upper()
#                 # symbols = symbols.replace('Ч', 'ч')
#                 # symbols = symbols.replace('Е', 'е')
#
#                 queryset = queryset.filter(name__icontains=symbols).order_by('name')
#             except ValueError:
#                 pass
#         return queryset
#
#     serializer_class = WormSerializer

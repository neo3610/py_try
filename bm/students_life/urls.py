from django.urls import path

from students_life.views import StudentList, StudentDetail, RoomList, RoomDetail, StudentInRoomList, StudentInRoomDetail

urlpatterns = [
    path('student/', StudentList.as_view()),
    path('student/<int:pk>/', StudentDetail.as_view()),
    path('room/', RoomList.as_view()),
    path('room/<int:pk>/', RoomDetail.as_view()),
    path('student_in_room/', StudentInRoomList.as_view()),
    path('student_in_room/<int:pk>/', StudentInRoomDetail.as_view()),
    ]
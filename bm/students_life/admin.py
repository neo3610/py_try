from django.contrib import admin

# Register your models here.
from students_life.models import Student, Room, StudentInRoom

admin.site.register(Student)
admin.site.register(Room)
admin.site.register(StudentInRoom)
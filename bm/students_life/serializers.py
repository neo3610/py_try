from rest_framework import serializers

from students_life.models import Student, Room, StudentInRoom


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'  # ['fio','group']
        model = Student


class RoomSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = Room


class StudentInRoomSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = StudentInRoom

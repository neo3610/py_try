from django.db import models


class Student(models.Model):
    fio = models.CharField(max_length=100)
    group = models.IntegerField()
    course = models.IntegerField()

    def __str__(self):
        return self.fio + ' группа: ' + str(self.group) + ' курс: ' + str(self.course)


class Room(models.Model):
    floor = models.IntegerField()
    capacity = models.IntegerField()

    def __str__(self):
        return 'комната на этаже №' + str(self.floor) + ' с вместимостью ' + str(self.capacity)


class StudentInRoom(models.Model):
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)

    def __str__(self):
        return 'комната ' + str(self.room) + ' со студентом ' + str(self.student)
